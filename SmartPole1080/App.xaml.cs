﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using SmartPole1080.View;

namespace SmartPole1080
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            InitResources();
        }


        protected override void OnStartup(StartupEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            //          mainWindow.GoFullscreen();
            base.OnStartup(e);
        }

        private void InitResources()
        {
            Resources = new ResourceDictionary();
            Resources.MergedDictionaries.Add(
                new ResourceDictionary
                {
                    Source = new Uri("/SmartPole.View;component/Assets/View.Resources.xaml", UriKind.RelativeOrAbsolute),
                });

            Resources.MergedDictionaries.Add(
                new ResourceDictionary
                {
                    Source = new Uri("/SoltaLabs.Avalon.View.Core;component/SoltaLabs.View.Core.Resources.xaml", UriKind.RelativeOrAbsolute),
                });
        }

    }
}

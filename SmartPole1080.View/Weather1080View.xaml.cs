﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using SmartPole.Model.OpenWeather;
using SoltaLabs.Avalon.Core.Helper;

namespace SmartPole1080.View
{
   /// <summary>
   /// Interaction logic for WeatherView.xaml
   /// </summary>
   public partial class Weather1080View : UserControl
   {
      public Weather1080View()
      {
         InitializeComponent();

         Loaded += WeatherView_Loaded;
      }

      private const string _currentUrl =
         @"http://api.openweathermap.org/data/2.5/weather?q={0}&appid=323383f81d554ace204a02d8ab49f874&mode=xml";

      private const string _forecastUrl =
         @"http://api.openweathermap.org/data/2.5/forecast?q={0}&mode=xml&appid=323383f81d554ace204a02d8ab49f874";

      private const string url = @"http://openweathermap.org/img/w/{0}.png";

      private void WeatherView_Loaded(object sender, System.Windows.RoutedEventArgs e)
      {
         //read weather

         txtToday.Text = DateTime.Now.ToString("dddd MMMM dd").ToUpper();

         var fileName = ConfigurationHelper.GetFileName("WeatherConfigFile");
         var content = ConfigurationHelper.ReadConfigurationContent(fileName);
         var config = XElement.Parse(content);

         //get current symbol
         try
         {
            var current = APIHelper.GetXmlReponse<current>(string.Format(_currentUrl, config.GetValue("Current")));
            BuildImg(currentImg, current.weather.icon);
         }
         catch (System.Exception)
         {

         }

         try
         {
            var forecast = APIHelper.GetXmlReponse<weatherdata>(string.Format(_forecastUrl, config.GetValue("ForeCast")));

            List<ForeCastItem> items = new List<ForeCastItem>();

            for (int i = 3; i < forecast.forecast.Length; i += 8)
            {
               items.Add(new ForeCastItem
               {
                  Day = forecast.forecast[i].from.DayOfWeek.ToString().Substring(0, 3).ToUpper(),
                  Image = MakeImg(forecast.forecast[i].symbol.var)
               });
            }

            ForeCaseList.ItemsSource = items;
         }
         catch (System.Exception)
         {

         }
      }

      public void BuildImg(Image imgage, string icon)
      {
         BitmapImage img = new BitmapImage();
         img.BeginInit();
         img.UriSource = new Uri(string.Format(url, icon), UriKind.Absolute);
         img.EndInit();

         imgage.BeginInit();
         imgage.Source = img;

         imgage.EndInit();
      }

      public BitmapImage MakeImg(string icon)
      {
         BitmapImage img = new BitmapImage();
         img.BeginInit();
         img.UriSource = new Uri(string.Format(url, icon), UriKind.Absolute);
         img.EndInit();

         return img;
      }

      public class ForeCastItem
      {
         public string Day { get; set; }
         public BitmapImage Image { get; set; }
      }
   }
}
﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using System.Windows.Threading;
using mshtml;
using SmartPole.ViewModel;
using SmartPole.ViewModel.Interfaces;
using SmartPole1080.ViewModel;
using SoltaLabs.Avalon.Core.Behaviors;
using SoltaLabs.Avalon.Core.Helper;
using SoltaLabs.Avalon.Core.Utilities;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SmartPole1080.View
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : IMainWindow, IIdleTarget
   {
      public MainWindow()
      {
         InitializeComponent();
         TimeoutInSecond = int.Parse(ConfigurationHelper.GetFileName("IdleTimeout"));

         var mainVm = new MainViewModel(this);
         DataContext = mainVm;

         ContentRendered += MainWindow_ContentRendered;
         KeyDown += MainWindow_KeyDown;
         Closing += MainWindow_Closing;
         PreviewMouseLeftButtonDown += Window_PreviewMouseLeftButtonDown;

         Loaded += (sender, e) => mainVm.BusStopViewModel.Start();
         Closing += (sender, e) => mainVm.BusStopViewModel.Close();

         SurveyView.LoadCompleted += SurveyView_LoadCompleted;

         SurveyView.Navigating += SurveyView_Navigating;
         //   SurveyView.Navigate(new Uri("https://www.surveymonkey.com/r/6SH5YWF"));
         //OverridesDefaultStyle = "False"
         //                                   ScrollViewer.CanContentScroll = "False"
         //                                   ScrollViewer.HorizontalScrollBarVisibility = "Hidden"
         //                                   ScrollViewer.VerticalScrollBarVisibility = "Hidden"      
      }

      private void SurveyView_LoadCompleted(object sender, NavigationEventArgs e)
      {
 //        var doc = (IHTMLDocument2)SurveyView.Document;
 //        if (doc?.body != null)
 //           doc.body.parentElement.style.overflow = "hidden";
      }

      private bool _manualTracking;

      private void SurveyView_Navigating(object sender, NavigatingCancelEventArgs e)
      {
         if(!_manualTracking)
            FeedbackPopup.IsOpen = false;
      }

      private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
      {
         if (cameraControl.IsCapturing)
            cameraControl.StopCapture();
      }

      private void MainWindow_KeyDown(object sender, KeyEventArgs e)
      {
         Window w = sender as Window;
         if (w != null)
         {
            if (e.Key == Key.F11 && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
               if (FullScreenManager.GetIsFullScreen(w))
               {
                  w.ExitFullScreen();
               }
               else
               {
                  w.GoFullscreen();
               }

               return;
            }
         }

         OnKeyDown(e);
      }

      private void MainWindow_ContentRendered(object sender, EventArgs e)
      {
         var allDevices = cameraControl.GetVideoCaptureDevices().ToArray();

         if (allDevices.Length > 0)
            cameraControl.StartCapture(allDevices[0]);
      }

      public void ToggleEmergency()
      {
         EmergencyPopup.IsOpen = !EmergencyPopup.IsOpen;
      }

      public void ToggleFeedback()
      {
         FeedbackPopup.IsOpen = !FeedbackPopup.IsOpen;
         if (FeedbackPopup.IsOpen)
         {
            _manualTracking = true;
            SurveyView.Navigate(new Uri("https://www.surveymonkey.com/r/6SH5YWF"));
            _manualTracking = false;
         }
         //if (FeedbackGrid.Visibility == Visibility.Visible)
         //   FeedbackGrid.Visibility = Visibility.Collapsed;
         //else if (FeedbackGrid.Visibility == Visibility.Collapsed)
         //{
         //   FeedbackGrid.Visibility = Visibility.Visible;
         //   SurveyView.Navigate(new Uri("https://www.surveymonkey.com/r/6SH5YWF"));
         //}
      }

      private void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
      {
         EmergencyPopup.IsOpen = false;
         FeedbackPopup.IsOpen = false;
      }

      private void Window_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
      {
         CloseEmergencyPopup();
      }

      private void CloseEmergencyPopup()
      {
         if (!EmergencyPopup.IsMouseOver && !EmergencyButton.IsMouseOver && EmergencyPopup.IsOpen)
            EmergencyPopup.IsOpen = false;
      }

      public ISquareView GetSquareView()
      {
         return theRollPanel.BottomContent as ISquareView;
      }

      public IHasBrowser GetJourneyView()
      {
         return theRollPanel.BodyContent as IHasBrowser;
      }

      public int TimeoutInSecond { get ; private set;  }
      public int CurrentIdleInSecond { get; set; }
      public DispatcherTimer Runner { get; set; }

      public void Reset()
      {
         (theRollPanel.BodyContent as IContentResetable)?.Reset();
         (theRollPanel.BottomContent as IContentResetable)?.Reset();

         EmergencyPopup.IsOpen = false;
         FeedbackPopup.IsOpen = false;
         _manualTracking = false;
      }
   }
}

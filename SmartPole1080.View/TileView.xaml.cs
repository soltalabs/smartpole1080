﻿using System;
using System.Linq;
using SmartPole.ViewModel.Interfaces;
using SoltaLabs.Avalon.Core;
using SoltaLabs.Avalon.Core.Attributes;
using SoltaLabs.Avalon.Core.Behaviors;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SmartPole1080.View
{
   /// <summary>
   /// Interaction logic for TileView.xaml
   /// </summary>
   public partial class TileView : ISquareView, IContentResetable
   {
      public TileView()
      {
         InitializeComponent();
      }

      public ISnapperView GetSnapperView()
      {
         return TileGrid.TileItems.OfType<ISnapperView>().FirstOrDefault();
      }

      public IHasBrowser GetView(Targets target)
      {
         var items = TileGrid.TileItems.OfType<IHasBrowser>();
         foreach (IHasBrowser hasBrowser in items)
         {
            var customAttributes = hasBrowser.GetType().GetCustomAttributes(typeof(TargetAttribute), true);

            var x = customAttributes.OfType<TargetAttribute>().FirstOrDefault();

            if (x != null && x.Target == target)
               return hasBrowser;
         }
         return null;
      }

      public void Reset()
      {
         TileGrid.Reset();
      }
   }
}

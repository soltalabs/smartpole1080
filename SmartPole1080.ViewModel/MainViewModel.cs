﻿using System.Windows.Input;
using SmartPole.ViewModel;
using SoltaLabs.Avalon.Core.Behaviors;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole1080.ViewModel
{
    public class MainViewModel : ObservableObject<MainViewModel>
   {
      private readonly IMainWindow _window;
      public MainViewModel(IMainWindow window)
      {
         _window = window;
         JourneyViewModel = new WebViewModel(_window.GetJourneyView());
         EmergencyCommand = new DelegateCommand(() => _window.ToggleEmergency());
         FeedbackCommand = new DelegateCommand(() => _window.ToggleFeedback());
         TileViewModel = new TileViewModel(_window.GetSquareView());
      }
      public BusStopViewModel BusStopViewModel { get; set; } = new BusStopViewModel(6);

      public TileViewModel TileViewModel { get; set; }

      public WebViewModel JourneyViewModel { get; set; }

      public ICommand EmergencyCommand { get; set; }
      public ICommand FeedbackCommand { get; set; }
   }
}

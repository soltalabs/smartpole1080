﻿using SmartPole.ViewModel;
using SmartPole.ViewModel.Interfaces;
using SoltaLabs.Avalon.Core;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole1080.ViewModel
{
   public class TileViewModel : ObservableObject<SquareViewModel>
   {
      public TileViewModel(ISquareView squareView)
      {
         EPayViewModel = new EPayViewModel();
         SnapperViewModel = new SnapperViewModel(squareView.GetSnapperView());
         EventFindaViewModel = new WebViewModel(squareView.GetView(Targets.EventFinda));
         CityOnViewModel = new WebViewModel(squareView.GetView(Targets.Cityon));
         WeatherViewModel = new WeatherViewModel();

         EPayViewModel.MoveToStep(0);
         SnapperViewModel.MoveToStep(0);
      }

      public EPayViewModel EPayViewModel { get; }

      public SnapperViewModel SnapperViewModel { get; }

      public WebViewModel EventFindaViewModel { get; }

      public WebViewModel CityOnViewModel { get; }

      public WeatherViewModel WeatherViewModel { get; }
   }
}

﻿using SoltaLabs.Avalon.Core.Utilities;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SmartPole1080.ViewModel
{
   public class WeatherViewModel : ObservableObject<WeatherViewModel>, IActiveViewModel
   {
      public void OnActivate(object args)
      {
      }

      public void OnDeactivate(object args)
      {
      }
   }
}
